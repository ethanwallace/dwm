# Ethan's DWM

This is my DWM config. There are no patches applied, however I have
changed the colours and added support for Thinkpad volume buttons. This
DWM is supposed to be used with my DWM startup scripts
[here](https://gitlab.com/ethanwallace/dwm-scripts). The font has also
been changed to [mononoki](https://madmalik.github.io/mononoki). 
